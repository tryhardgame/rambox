﻿using UnityEngine;
using DG.Tweening;

//[AdvancedInspector]
public class Box : MonoBehaviour {

    float scale = 1f;
    public float maxScaleFactor;
    public float minScaleFactor;

    public float speed;
    public float maxSpeed;
    public float minSpeed;

    public float jumpStrength;
    public float maxJumpStrength;
    public float minJumpStrength;

    float scaleRate;
    public float scaleUpRate;
    public float scaleDownRate;
    public bool uniformScaleRate = true;

    float speedChangeRate;
    public float speedUpRate;

    public float speedDownRate;
    public bool uniformSpeedingRate = true;

    Camera Cam;

    bool hasUniformSpeedingRate()
    {
        return uniformSpeedingRate;
    }

    void OnCollisionEnter2D(Collision2D Coll)
    {
        if(Coll.gameObject.GetComponent<Wall>()!= null)
        {
            isJumpReady = true;
        }
    }

    void OnTriggerEnter2D(Collider2D Coll)
    {

    }

    public void OnFingerDown()
    {
        ShrinkBox();
    }

    public void OnFingerUp()
    {
        GrowBox();
    }

    Vector2 maxScale;
    Vector2 minScale;

    void GrowBox()
    {
        DOTween.Kill("shrink"); //Stop Shrinking Before Growing

        scaleRate = scaleUpRate;
        transform.DOScale(maxScale, scaleRate).SetId("grow"); //Increase Scale

        speedChangeRate = speedDownRate;
        DOTween.To(() => speed, x => speed = x, minSpeed, speedChangeRate).SetId("grow"); //Decrease Speed       
    }

    void ShrinkBox()
    {
        DOTween.Kill("grow"); //Stop Growing Before Shrinking

        scaleRate = scaleDownRate;
        transform.DOScale(minScale, scaleRate).SetId("shrink"); //Decrease Scale

        speedChangeRate = speedUpRate;
        DOTween.To(() => speed, x => speed = x, maxSpeed, speedChangeRate).SetId("shrink"); //Increase Speed
    }

    public void Jump()
    {
        if( _Rigidbody2D!= null)
        {
            if(isJumpReady)
            {
                _Rigidbody2D.AddForce(Vector2.up * jumpStrength);
                isJumpReady = false;
            }
        }
    }

    public bool isMoveReady = true;
    bool isJumpReady = true;

    void Move()
    {
        transform.Translate(Vector2.right * Time.deltaTime * speed);
        //_Rigidbody2D.velocity = Vector2.right * speed;
    }
  
    Rigidbody2D _Rigidbody2D;
    Renderer _Renderer;

    void Awake()
    {
        _Rigidbody2D = GetComponent<Rigidbody2D>();
        _Renderer = GetComponent<Renderer>();
    }

    void Start () {

        Cam = Camera.main;

        maxScale = transform.localScale * maxScaleFactor;
        minScale = transform.localScale * minScaleFactor;

        if(uniformScaleRate)
        {

        }

        if(uniformSpeedingRate)
        {

        }	
	}

	void Update () {

        if(isMoveReady)
        {
            Move();
        }
        //oif
        //if(Input.GetTouch(0).phase == TouchPhase.Began)
        //{
        //    ShrinkBox();
        //}

        //if (Input.GetTouch(0).phase == TouchPhase.Ended)
        //{
        //    GrowBox();
        //}

        if(Input.GetMouseButtonDown(0))
        {
            ShrinkBox();
        }

        if(Input.GetMouseButtonUp(0))
        {
            GrowBox();
        }

        if(Input.GetKeyDown(KeyCode.Space))
        {
            Jump();
        }

        if (!(GeometryUtility.TestPlanesAABB(GeometryUtility.CalculateFrustumPlanes(Cam), _Renderer.bounds)))
        {
            //Reset
        }
    }
}
