﻿using UnityEngine;

public class TrackingCamera : MonoBehaviour {

    public float dampTime = 0.15f;
    private Vector3 velocity = Vector3.zero;
    public Transform target;

    public float cameraCenterScaleX = 0.25f;
    public float cameraCenterScaleY = 0.5f;

    tk2dCamera cam;
    Camera camera;

    public float zoomFactor = 1f;
    Bounds gameBounds = new Bounds();

    public bool TrackX;
    public bool TrackY;

    void CalculateGameBounds()
    {
        Wall[] Walls = FindObjectsOfType<Wall>();

        gameBounds = new Bounds(Vector2.zero, Vector2.one);

        foreach (Wall w in Walls)
        {
            if(w._SpriteRenderer!=null)
            {
                gameBounds.Encapsulate(w._SpriteRenderer.bounds);
            }
        }
    }

    void Awake()
    {
        cam = GetComponent<tk2dCamera>();
        camera = cam.GetComponent<Camera>();
    }

    void Start()
    {
        CalculateGameBounds();
    }


    void Update()
    {
        if (target)
        {
            Vector3 point = camera.WorldToViewportPoint(target.position);
            Vector3 delta = target.position - camera.ViewportToWorldPoint(new Vector3(cameraCenterScaleX, cameraCenterScaleY, point.z)); //(new Vector3(0.5, 0.5, point.z));
            Vector3 destination = transform.position + delta;

            Vector3 cameraRectMin = camera.ViewportToWorldPoint(camera.rect.min);
            Vector3 cameraRectMax = camera.ViewportToWorldPoint(camera.rect.max);

            float height = cameraRectMax.y - cameraRectMin.y;
            float width = cameraRectMax.x - cameraRectMin.x;

            Vector3 newDestination = new Vector3(Mathf.Clamp(destination.x, gameBounds.min.x + width / 2, gameBounds.max.x - width / 2), transform.position.y, destination.z);

            transform.position = Vector3.SmoothDamp(transform.position, newDestination, ref velocity, dampTime);

            cam.ZoomFactor = zoomFactor;
        }
    }
}
